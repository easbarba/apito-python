<!-- 
 apito-python is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 apito-python is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with apito-python. If not, see <https://www.gnu.org/licenses/>.
-->

# TODO

## HIGH

- leverage symfony serializer to json objects


## MEDIUM
- timestamptable instead of created At (stof doctrine extensions)
- sluggable[string | 100 length]  (stof doctrine extensions)
- pagination babdev/pagerfanta-bundle pagerfanta/doctrine-orm-adapter
