# apito-python is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-elysia is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-elysia. If not, see <https://www.gnu.org/licenses/>.

"""API main caller."""

from typing import Dict

from starlette import status

from apito.app import app


@app.get("/", status_code=status.HTTP_200_OK, response_model=Dict[str, str])
def get_home() -> Dict[str, str]:
    """Welcome home page."""
    return {"message": "Welcome to Apito"}
