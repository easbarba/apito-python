FROM python:3.11
MAINTAINER EAS Barbosa <easbarba@outlook.com>
WORKDIR /app
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN python -m venv venv
RUN bash ./venv/bin/activate
RUN pip install -r requirements.txt
COPY . .
CMD [ "uvicorn", "apito.app:app", "--port", "8080", "--host", "0.0.0.0", "--reload" ]
