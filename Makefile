# Apito-Python is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Apito-Python is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Apito-Python. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*

NAME := apito-python
VERSION := $(shell cat .version)
RUNNER ?= podman
BACKEND_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ------------------------------------
base: initial database #server

stats:
	${RUNNER} pod stats ${POD_NAME}

stop:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

initial:
	# ----------- CREATE POD
	${RUNNER} pod create \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

database:
	# ----------- ADD DATABASE
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

server:
	# ----------- SERVER
	${RUNNER} rm -f ${SERVER_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

prod:
	# ----------- BACKEND PROD
	${RUNNER} rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--rm \
		--detach \
		--name ${NAME}-prod \
		--env-file ./envs/.env.db \
		${BACKEND_IMAGE}

start:
	# ----------- BACKEND DEV
	${RUNNER} rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE}

repl:
	# ----------- BACKEND REPL
	${RUNNER} rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-repl \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash

test-unit:
	# ----------- BACKEND DEV
	${RUNNER} rm -f ${NAME}-test-unit
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-unit \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c "composer run test:unit -n"

test-integration:
	# ----------- BACKEND REPL
	${RUNNER} rm -f ${NAME}-test-integration
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-integration \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c "./bin/test && composer run test:integration -n"


test-all:
	# ----------- BACKEND REPL
	${RUNNER} rm -f ${NAME}-test-integration
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-integration \
		--env-file ./envs/.env.db \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}/ops/php-fpm.conf:/usr/local/etc/php-fpm.d/zzz-docker.conf:Z \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c "composer run tests -n"

api:
	./apitest | jq

repl-db:
	# ----------- DATABASE REPL
	podman exec -it ${DATABASE_NAME} ${SQL_REPL_CMD}

cmd:
	${RUNNER} run \
		--rm --tty --interactive \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--workdir ${BACKEND_FOLDER} \
		${BACKEND_IMAGE} \
		bash -c '$(shell cat container-commands | fzf)'

build:
	# ---------------------- BUILD BACKEND IMAGE
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}

publish:
	# ---------------------- PUBLISH BACKEND IMAGE
	${RUNNER} push ${BACKEND_IMAGE}

	# ---------------------- PUBLISH DATABASE IMAGE
	# ${RUNNER} commit ${DATABASE_NAME} ${GITLAB_REGISTRY}/${USER}/${DATABASE_NAME}:${VERSION}
	# ${RUNNER} push ${GITLAB_REGISTRY}/${USER}/${DATABASE_NAME}:${VERSION}

openapi:
	curl localhost:5000/openapi.json > docs/openapi/${NAME}-openapi-${VERSION}.json

guix:
	guix shell --pure --container

.PHONY := base start stop prod server database cmd publish build repl repl-db stats openapi
.DEFAULT_GOAL := test
