<!-- 
 apito-python is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 apito-python is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with apito-python. If not, see <https://www.gnu.org/licenses/>.
 -->

# CONTRIBUTING

Contributions are welcome via pull request.

## Guidelines

Please make sure to successfully check-up the guidelines list before sending a PR:

- run `./prfix.bash`
- run tests
- add due entries changes in `CHANGELOG.md`
- tag per [SemVer](http://semver.org/).
